const Cookie = process.client ? require('js-cookie') : undefined
export default function ({ store, redirect }) {
    // If the user is not authenticated
    if(Cookie.get('name')){
      store.commit('SET_NAME', Cookie.get('name'))
    }
    console.log("middleware",store.state.name)
    if (!store.state.name) {
      store.commit("TELL_NAME_DIALOG")
    }
  }
  