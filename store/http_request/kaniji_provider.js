import HttpRequest from './http_request'
import FirebaseDbProvider from './firebase_db_provider'
const { Kanjiapi } = require('kanjiapi-wrapper')
const FirebaseDbService = new FirebaseDbProvider();
class KanjiProvider  {
  constructor () {
    // api api
    // super('kanji')
  }
  async getKanji (kanji) {
    var from_firebase = await FirebaseDbService.getKanji(kanji);
    if(from_firebase) return from_firebase;
    return new Promise(async (resolve,reject)=>{
      const kanjiapi = Kanjiapi.build()
      kanjiapi.addListener('listener_name', async ()=>{
          var success_result = await kanjiapi.getKanji(kanji)
          if(success_result.status == "SUCCESS"){
            FirebaseDbService.saveKanji(success_result.value.kanji,success_result.value);
            resolve(success_result.value);
          }
          else resolve(null);
      })
      const result = await kanjiapi.getKanji(kanji)
    })
  }
  async randomVerb() {
    return await FirebaseDbService.randomVerb();
  }
  async randomAdjective() {
    return await FirebaseDbService.randomAdjective();
  }
  async saveVerb(verb,object,auth){
    if(verb.toString().slice(-2) == "ます")
      return await FirebaseDbService.saveVerb(verb,object,auth);
    else return -1;
  }
  async saveAdjective(adjective,object,auth){
    return await FirebaseDbService.saveAdjective(adjective,object,auth);
  }
}

export default KanjiProvider