import {firebase_db} from '~/plugins/firebaseConfig';
import firebase from 'firebase/app'



class FirebaseDbProvider  {
  constructor () {
  }
  async saveAccount(name){
    var t = new Date();
    var object_save = {
      name,
      date:t.getTime(),
      language:navigator.language,
      platform:navigator.platform
    }
    return new Promise(async (resolve,reject)=>{
      firebase_db.collection("account").doc(`${name}-${t.getTime()}`).set(object_save)
      .then(function() {
          console.log("Document successfully written!");
          resolve(`${name}-${t.getTime()}`);
      })
      .catch(function(error) {
          console.error("Error writing document: ", error);
          resolve(0);
      });
  })
  }
  async getKanji (object_id) {
  
    return new Promise(async (resolve,reject)=>{
        firebase_db.collection("kanji")
        .doc(object_id)
        .onSnapshot(function(doc) {
            resolve(doc.data());
        });
    })
  }
  async saveKanji (object_id,object) {
    return new Promise(async (resolve,reject)=>{
        firebase_db.collection("kanji").doc(object_id).set(object)
        .then(function() {
            console.log("Document successfully written!");
            resolve(1);
        })
        .catch(function(error) {
            console.error("Error writing document: ", error);
            resolve(0);
        });
    })
    
  }
  async randomVerb () {
    return new Promise(async (resolve,reject)=>{
      firebase_db.collection('verb').get()
          .then((snapshot) => {
            resolve(snapshot.docs[Math.floor(Math.random() * snapshot.size)].data());
          })
          .catch((err) => {
            console.log('Error getting documents', err);
          });
    })
  }
  async randomAdjective () {
  
    return new Promise(async (resolve,reject)=>{
      firebase_db.collection('adjective').get()
          .then((snapshot) => {
            resolve(snapshot.docs[Math.floor(Math.random() * snapshot.size)].data());
          })
          .catch((err) => {
            console.log('Error getting documents', err);
          });
    })
  }
  async saveVerb (object_id,object,auth) {
    return new Promise(async (resolve,reject)=>{
        object["auth"] = auth;
        firebase_db.collection("verb").doc(object_id).set(object)
        .then(function() {
            console.log("Document successfully written!");
            resolve(1);
        })
        .catch(function(error) {
            console.error("Error writing document: ", error);
            resolve(0);
        });
    })
  }
  async saveAdjective (object_id,object,auth) {
    return new Promise(async (resolve,reject)=>{
        object["auth"] = auth;
        firebase_db.collection("adjective").doc(object_id).set(object)
        .then(function() {
            console.log("Document successfully written!");
            resolve(1);
        })
        .catch(function(error) {
            console.error("Error writing document: ", error);
            resolve(0);
        });
    })
  }
}

export default FirebaseDbProvider