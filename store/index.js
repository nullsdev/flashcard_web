import KanjiProvider from '~/store/http_request/kaniji_provider';
import FirebaseDbProvider from '~/store/http_request/firebase_db_provider'
const Cookie = process.client ? require('js-cookie') : undefined
const KanjiService = new KanjiProvider();
const FirebaseDbService = new FirebaseDbProvider();
var _kanjies = require("~/assets/kanji.json");
var _verbs = require("~/assets/verb.json");
var _adjectives = require("~/assets/adjective.json");
var _allkanji = require("~/assets/allkanji.json");
var kanji_levels = [
    require("~/assets/kanji1.json"),
    require("~/assets/kanji2.json"),
    require("~/assets/kanji3.json"),
    require("~/assets/kanji4.json"),
    require("~/assets/kanji5.json"),
    require("~/assets/kanji6.json"),
    require("~/assets/kanji8.json")
]
const KANJI = 0;
const VERB = 1;
const ADJECTIVE = 2;
export const strict = false;
export const state = () => ({
    tellname_dialog:false,
    name:null,
    kanjies: _kanjies,
    kanji:null,
    kanji_level:[],

    verbs:_verbs,
    adjectives:_adjectives,

    catagory:KANJI,//0 kanji, 1 verb, 3 adjective
    lookup:true,
    lookup_kotoba:true

    ,allkanji:_allkanji
    ,kanji_detail:null

    ,recommend_type:VERB
    ,recommend_dialog:false
    ,recommend_verb:{
        status:"pending",
        kanji: null,
        hira:null,
        level: 1,
        url: null,
        translations: null,
        kotobas:[
            // {
            //     "kanji":"車をうんてんします",
            //     "hira":"くるまをうんてんします",
            //     "mean":"ขับรถ"
            // }
        ]
    }
    ,recommend_adjective:{
        status:"pending",
        kanji: null,
        hira:null,
        level: 1,
        url: null,
        translations: null,
        type:null,
        kotobas:[
            // {
            //     "kanji":"車をうんてんします",
            //     "hira":"くるまをうんてんします",
            //     "mean":"ขับรถ"
            // }
        ]
    }
    ,snackbar:{
        show:false,
        message:'',
        color:'',
        icon:''
    },
})

export const mutations = {
    TELL_NAME_DIALOG(state){
        state.tellname_dialog = true;
    },
    CLOSE_TELL_NAME_DIALOG(state){
        state.tellname_dialog = false;
    },
    SET_NAME(state,name){
        state.name = name;
    },
    SUCCESS(state,message){
        state.snackbar.show = true
        state.snackbar.message = message
        state.snackbar.color = 'success',
        state.snackbar.icon= 'mdi-emoticon'

    },
    FAIL(state,message){
        state.snackbar.show = true
        state.snackbar.message = `${message}`
        state.snackbar.color = 'red',
        state.snackbar.icon= 'mdi-emoticon-sad'
    },
    SET_SNACKBAR(state,status){
        state.snackbar.show = status
    },
    SET_KANJI (state,val) {
        state.kanji = state.kanjies[val];//Math.floor(Math.random() * this.kanjies.length);
    },
    SET_NEW_KANJI (state,val) {
        state.allkanji = val;
    },
    SET_VERB (state,val) {
        state.kanji = val;//Math.floor(Math.random() * this.kanjies.length);
    },
    SET_ADJ (state,val) {
        state.kanji = val;//Math.floor(Math.random() * this.kanjies.length);
    },
    SET_CAT(state,val) {
        state.catagory = val;
    },
    TOGLE_LOOKUP(state){
        state.lookup = !state.lookup;
    },
    TOGLE_LOOKUP_KOTOBA(state){
        state.lookup_kotoba = !state.lookup_kotoba;
    },
    SET_KANJI_DETAIL(state,detail){
        state.kanji_detail = detail;
    },
    SET_RECOMMEND_TYPE (state,val) {
        state.recommend_type = val;
    },
    SET_RECOMMEND_DIALOG (state,val) {
        state.recommend_dialog = val;
    }

}
export const actions = {
    async saveName({commit},val) {
        var name_from_base = await FirebaseDbService.saveAccount(val);
        if(name_from_base){
            Cookie.set("name",val);
            Cookie.set("auth",name_from_base);
            commit("SET_NAME",val);    
        }else{
            console.log("Save account error!");
        }
        commit("CLOSE_TELL_NAME_DIALOG");
    },
    setKanujiLevel({commit},val){
        if(val == 'All'){
            commit("SET_NEW_KANJI",_allkanji)
            return;
        }
        // var new_kanji = val.reduce((res,i)=>{
        //     return res.concat(kanji_levels[i-1]);
        // },[])
        commit("SET_NEW_KANJI",kanji_levels[val-1]);
    },
    async getNext({commit,state}) {
        let v = null;
        switch(state.catagory){
            case KANJI:
                v = Math.floor(Math.random() * state.allkanji.length);
                var d = await KanjiService.getKanji(state.allkanji[v]);
                commit("SET_KANJI_DETAIL",d);
            break;
            case VERB:
                commit('SET_VERB',await KanjiService.randomVerb());
            break;
            case ADJECTIVE:
                commit('SET_ADJ',await KanjiService.randomAdjective());
            break;
        }
    },
    randomKanji({commit,state}) {
        let v = Math.floor(Math.random() * state.allkanji.length);
        commit('SET_KANJI',v);
    },
    setKanji({commit,state},key) {
        commit('SET_CAT',KANJI);
        let v = state.kanjies.map(m=>m.kanji).indexOf(key);
        commit('SET_KANJI',v);
    },
    setVerb({commit,state},key) {
        let v = state.verbs.map(m=>m.kanji).indexOf(key);
        commit('SET_VERB',v);
    },
    setAdj({commit,state},key) {
        let v = state.adjectives.map(m=>m.kanji).indexOf(key);
        commit('SET_ADJ',v);
    },
    
    async getKanjiDetail({commit,state},kanji) {
        var d = await KanjiService.getKanji(kanji);
        commit("SET_KANJI_DETAIL",d);
    },
    recommendUs({commit,state},val) {
        commit("SET_RECOMMEND_TYPE",val);
        commit("SET_RECOMMEND_DIALOG",true);
    },
    closeRecommendDialog({commit}) {
        commit("SET_RECOMMEND_DIALOG",false);
    },
    async saveRecommend({commit,state}){
        var _auth = Cookie.get("auth") ;
        _auth = _auth ? _auth : `Guest`
        switch(state.recommend_type){
            case KANJI:
            break;
            case VERB:
                if(await KanjiService.saveVerb(state.recommend_verb.hira,state.recommend_verb,_auth) == -1)
                    commit("FAIL","Only ますけ　please");
                else{
                    commit("SUCCESS","おわりました。ありがとうございます。");
                }
            break;
            case ADJECTIVE:
                await KanjiService.saveAdjective(state.recommend_adjective.hira,state.recommend_adjective,_auth)
                commit("SUCCESS","おわりました。ありがとうございます。");
            break;
        }
        commit("SET_RECOMMEND_DIALOG",false);
    }
}
export const getters = {
    getNameOfKanjies : state=>{
       return state.kanjies.map(k=>{
           return{
               img : k.kanji,
               name: k.kun_readings ? k.kun_readings[0].reading:(k.on_readings ? k.on_readings[0].reading:'-')
           }
       }) 
    }
}