import firebase from 'firebase/app'
import 'firebase/firestore'
import 'firebase/analytics'

// Initialize Cloud Firestore through Firebase
if (!firebase.apps.length) {
    var firebaseConfig = {
        apiKey: "AIzaSyD2Vzhc_fOUl8o7bBVA-rLx6bSI9cCUHLc",
        authDomain: "jp-flashcard.firebaseapp.com",
        databaseURL: "https://jp-flashcard.firebaseio.com",
        projectId: "jp-flashcard",
        storageBucket: "jp-flashcard.appspot.com",
        messagingSenderId: "934509671017",
        appId: "1:934509671017:web:5ffa9063aa212c34aa68f8",
        measurementId: "G-0NEMZM3ZG3"
    };
    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);
    firebase.analytics();
}

export const firebase_db = firebase.firestore()